package com.mitocode.repo;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mitocode.model.Consulta;

@Repository
public interface IConsultaRepo extends JpaRepository<Consulta, Integer>{
	
	@Query("FROM Consulta c WHERE c.paciente.dni=:dni "
			+ "OR LOWER(c.paciente.nombres) LIKE %:nombreCompleto% "
			+ "OR LOWER(c.paciente.apellidos) LIKE %:nombreCompleto%")
	 List<Consulta> buscar(@Param("dni")String dni, @Param("nombreCompleto") String nombreCompleto);
	
	// Izquierdo: >= Derecho <
	@Query("FROM Consulta c WHERE c.fecha between :fechaConsulta AND :fechaSgte ")
	List<Consulta> buscarFecha(LocalDateTime fechaConsulta, LocalDateTime fechaSgte);
	
	@Query(value = "select * from fn_listarresumen()", nativeQuery = true)
	List<Object[]> listarResumen();

}
