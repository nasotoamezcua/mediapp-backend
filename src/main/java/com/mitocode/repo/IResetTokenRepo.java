package com.mitocode.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mitocode.model.ResetToken;

@Repository
public interface IResetTokenRepo extends JpaRepository<ResetToken, Integer> {
	
	//from ResetToken rt where rt.token = :?
	ResetToken findByToken(String token);

}
